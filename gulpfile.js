var gulp = require('gulp');
var less = require('gulp-less');
var cssmin = require('gulp-cssmin');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');

gulp.task('default', ['minify-css', 'minify-js']);

//TODO concat JS i CSS

// Minify JS
gulp.task('minify-js', function(){
    return gulp.src(['./scripts/*.js', '!./scripts/*.min.js'])
        .pipe(uglify())
        .pipe(rename({suffix:'.min'}))
        .pipe(gulp.dest('./scripts/'));
});

// Minify CSS
gulp.task('minify-css', ['compile-less'], function(){
    return gulp.src(['./css/*.css', '!./css/*.min.css'])
        .pipe(cssmin())
        .pipe(rename({suffix:'.min'}))
        .pipe(gulp.dest('./css/'));
});

/* Task to compile less */
gulp.task('compile-less', function() {
    return gulp.src('./css/*.less')
        .pipe(less())
        .pipe(gulp.dest('./css/'));
});